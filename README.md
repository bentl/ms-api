# MiniSend frontend

## Setup
1. Copy .env.example to .env

2. Run from the project root:


    docker-composer up

3. Add the following to hosts file:


    127.0.0.1 mini-email-sender.api.docker.test

4. Connect to container `mini-email-sender-api-app` and run if migration are not executed during up:
    

    docker exec -it mini-email-sender-api-app sh
    cd /var/www/html && php artisan migrate

To seed database with fake data run inside `mini-email-sender-api-app` container:

    docker exec -it mini-email-sender-api-app sh
    cd /var/www/html && php artisan db:seed --class=EmailActivitySeeder

To handle job run inside `mini-email-sender-api-app` container:

    docker exec -it mini-email-sender-api-app sh
    cd /var/www/html && php artisan queue:work

At `http://localhost:15672/` rabbitmq manager is located
Username and password can be taken from environment variables

## Details

Implemented API with the following features:

1) Create emails with dispatching EmailSendJob via rabbitmq. 
    Actual email is not sent, just delay with 2 sec is added on dispatching.
    Subject, sender, recipient, text content, html content and attacment(s) are stored; 
    
2) Fetching list of emails with pagination, filter by recipient, sender, subject;

3) View of email with previously sent data on emails creation. Attachments are returned as file names array;

4) Fetching analytics data about total amount per status, avg time for handling job,

## Tests

        docker exec -it mini-email-sender-api-app sh
        cd /var/www/html && ./vendor/bin/phpunit
