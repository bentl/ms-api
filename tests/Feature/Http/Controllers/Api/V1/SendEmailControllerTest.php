<?php

declare(strict_types=1);

namespace Tests\Feature\Http\Controllers\Api\V1;

use App\Models\EmailActivity;
use App\Models\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class SendEmailControllerTest extends \Tests\TestCase
{

    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_sending_email_success()
    {
        $id = 31;
        $recipient = 'test@test.com';
        $mail = Mail::factory()
            ->has(EmailActivity::factory())
            ->createOne(['id' => $id, 'recipient' => $recipient]);

        $this->assertDatabaseCount('mails', 1);
        $this->assertDatabaseCount('email_activities', 1);
        $this->assertDatabaseHas('email_activities', [
            'mail_id' => $id,
        ]);
        $this->assertDatabaseHas('mails', [
            'recipient' => $recipient,
        ]);
        $this->withHeaders(['Accept' => 'application/json'])
            ->post('api/v1/send_emails', $mail->toArray())
            ->assertNoContent();
    }
}
