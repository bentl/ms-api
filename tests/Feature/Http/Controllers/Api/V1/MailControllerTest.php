<?php

declare(strict_types=1);

namespace Tests\Feature\Http\Controllers\Api\V1;

use App\Models\EmailActivity;
use App\Models\Mail;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MailControllerTest extends \Tests\TestCase
{

    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
    }

    public function test_fetching_mail_list_no_filters()
    {
        $perPage = 5;
        $page = 2;
        $total = 11;
        Mail::factory()
            ->count($total)
            ->has(EmailActivity::factory())
            ->create();
        $this->assertDatabaseCount('mails', 11);
        $this->assertDatabaseCount('email_activities', 11);
        $this->get('api/v1/mails?' . http_build_query(['page' => $page, 'perPage' => $perPage]))
            ->assertSuccessful()
            ->assertJson(['per_page' => $perPage, 'current_page' => 2, 'total' => $total]);
    }

    public function test_get_mail_success()
    {
        $id = 31;
        $recipient = 'test@test.com';
        $factory = EmailActivity::factory();
        $mail = Mail::factory()
            ->has($factory)
            ->createOne(
                [
                    'id' => $id,
                    'recipient' => $recipient
                ]
            );
        $this->get('api/v1/mails/' . $id)
            ->assertSuccessful()
            ->assertJson($mail->toArray());
    }

    public function test_get_mail_not_found()
    {
        $id = 31;
        $this->get('api/v1/mails/' . $id)
            ->assertNotFound();
    }
}
