<?php

namespace Filters;

use App\Filters\MailFilter;
use App\Filters\MailFilterFactory;
use Tests\TestCase;

class MailFilterFactoryTest extends TestCase
{


    public function test_making_full_filter_from_array()
    {
        $filter = (new MailFilterFactory())->makeFilterFromRequestData(
            [
                'page' => 1,
                'perPage' => 23,
                'recipient' => 'a',
                'sender' => 'b',
                'subject' => 'c'
            ]
        );
        $this->assertInstanceOf(MailFilter::class, $filter);
    }

    public function test_making_empty_filter_from_array()
    {
        $filter = (new MailFilterFactory())->makeFilterFromRequestData([]);
        $this->assertInstanceOf(MailFilter::class, $filter);
        $this->assertTrue(is_null($filter->recipient));
        $this->assertTrue(is_null($filter->sender));
        $this->assertTrue(is_null($filter->subject));
        $this->assertEquals(10, $filter->perPage);
    }


    public function test_making_single_field_filter_from_array()
    {
        $recipientFilterValue = 'test';
        $filter = (new MailFilterFactory())->makeFilterFromRequestData(['recipient' => $recipientFilterValue]);
        $this->assertInstanceOf(MailFilter::class, $filter);
        $this->assertTrue(is_null($filter->sender));
        $this->assertTrue(is_null($filter->subject));
        $this->assertEquals($recipientFilterValue, $filter->recipient);
        $this->assertEquals(10, $filter->perPage);
    }

}
