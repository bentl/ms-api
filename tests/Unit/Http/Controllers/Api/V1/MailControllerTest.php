<?php

declare(strict_types=1);

namespace Tests\Unit\Http\Controllers\Api\V1;

use App\Filters\MailFilter;
use App\Models\EmailActivity;
use App\Models\Mail;
use App\Repositories\MailRepository;
use Illuminate\Pagination\LengthAwarePaginator;

/**
 * Class MailControllerTest
 * @covers \App\Http\Controllers\Api\V1\MailController
 * @coversDefaultClass \App\Http\Controllers\Api\V1\MailController
 */
class MailControllerTest extends \Tests\TestCase
{

    private $mailRepositoryMock;

    public function setUp(): void
    {
        parent::setUp();
        $this->mailRepositoryMock = $this->createMock(MailRepository::class);
        $this->app->instance(MailRepository::class, $this->mailRepositoryMock);
    }

    /**
     * @covers ::index
     */
    public function test_index_success()
    {
        $total = 4;
        $perPage = 2;
        $page = 2;
        $mails = Mail::factory()
            ->count($total)
            ->make([
                'emailActivity' => EmailActivity::factory()->make()
            ]);
        $paginatedResponse =  new LengthAwarePaginator($mails, $mails->count(), $perPage, $page);

        $this->mailRepositoryMock->expects($this->once())
            ->method('findAllByFilter')
            ->willReturn($paginatedResponse);
        $expected = $paginatedResponse->toArray();
        $this->get('api/v1/mails?' . http_build_query(['page' => $page, 'perPage' => $perPage]))
            ->assertExactJson($expected)
            ->assertSuccessful();
    }

    /**
     * @covers ::index
     */
    public function test_index_default_page_and_per_page()
    {
        $total = 4;
        $mails = Mail::factory()
            ->count($total)
            ->make([
                'emailActivity' => EmailActivity::factory()->make()
            ]);
        $perPage = (new MailFilter())->perPage;
        $paginatedResponse =  new LengthAwarePaginator($mails, $mails->count(), $perPage);

        $this->mailRepositoryMock->expects($this->once())
            ->method('findAllByFilter')
            ->willReturn($paginatedResponse);
        $expected = $paginatedResponse->toArray();
        $this->get('api/v1/mails')
            ->assertExactJson($expected)
            ->assertJson(['per_page' => $perPage, 'current_page' => 1]);
    }

    /**
     * @covers ::get
     */
    public function test_get_item_success()
    {
        $id = 1;
        $mail = Mail::factory()
            ->make([
                'attachments' => [],
                'id' => 1,
                'emailActivity' => null
            ]);

        $this->mailRepositoryMock->expects($this->once())
            ->method('find')
            ->willReturn($mail);
        $this->get('api/v1/mails/' . $id)
            ->assertExactJson($mail->toArray());
    }

    /**
     * @covers ::get
     */
    public function test_get_not_found()
    {
        $id = 1;
        $this->mailRepositoryMock->expects($this->once())
            ->method('find')
            ->willReturn(null);
        $this->get('api/v1/mails/' . $id)
            ->assertStatus(404);
    }

}
