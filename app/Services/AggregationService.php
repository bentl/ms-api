<?php


namespace App\Services;


use Illuminate\Support\Facades\DB;

class AggregationService
{
    public function getAnalytics()
    {
        $statusesTotal = DB::table('email_activities')
            ->select(
                'status',
                DB::raw('count(*) as total'),
            )
            ->groupBy('status')
            ->get();
        $activitiesStats = DB::table('email_activities')
            ->select(
                DB::raw('count(*) as totalActivities'),
                DB::raw('AVG(TIMESTAMPDIFF(SECOND, email_activities.created_at, email_activities.updated_at)) as delay')
            )
            ->get();

        return [
            'statuses' => $statusesTotal,
            'total' => $activitiesStats[0] ?? []
        ];


    }

}
