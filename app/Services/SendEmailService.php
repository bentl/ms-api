<?php

namespace App\Services;

use App\Jobs\EmailSendJob;
use App\Models\EmailActivity;
use App\Repositories\AttachmentRepository;
use App\Repositories\MailRepository;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\DB;

class SendEmailService
{
    /**
     * @var MailRepository
     */
    private MailRepository $mailRepository;
    /**
     * @var AttachmentRepository
     */
    private AttachmentRepository $attachmentRepository;

    public function __construct(MailRepository $mailRepository, AttachmentRepository $attachmentRepository)
    {
        $this->mailRepository = $mailRepository;
        $this->attachmentRepository = $attachmentRepository;
    }

    //todo: make class instead of array
    public function send($data): void
    {
        DB::transaction(function () use ($data) {
            $mail = $this->mailRepository->create($data);
            foreach (Arr::get($data, 'attachments', []) as $attachment) {
                $this->attachmentRepository->create($attachment, $mail);
            }
            $activity = $mail->emailActivity()->create([
                'status' => EmailActivity::POSTED_STATUS
            ]);
            EmailSendJob::dispatch($activity)
                ->delay(now()->addSeconds(2));
        });
    }

}
