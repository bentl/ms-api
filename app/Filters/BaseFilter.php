<?php

declare(strict_types=1);

namespace App\Filters;

abstract class BaseFilter
{
    public int $perPage = 10;

    abstract public function isFieldFilterSet(): bool;

    public function castFilterParam(string $propName, $paramValue)
    {
        $transformers = $this->getParamCasters();
        if (property_exists($this, $propName) && isset($transformers[$propName])) {
            return $transformers[$propName]($paramValue);
        }
        return $paramValue;
    }

    /** @return callable[] */
    protected function getParamCasters(): array
    {
        return [
            'perPage' => static function ($value) {
                return (int)$value;
            }
        ];
    }

}
