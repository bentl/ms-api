<?php

declare(strict_types=1);

namespace App\Filters\Interfaces;

use App\Filters\BaseFilter;

interface FilterFactoryInterface
{
    public function makeFilterFromRequestData(array $requestData): BaseFilter;
}
