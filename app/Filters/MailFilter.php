<?php

declare(strict_types=1);

namespace App\Filters;

class MailFilter extends BaseFilter
{
    public ?string $recipient = null;

    public ?string $subject = null;

    public ?string $sender = null;

    /**
     * Check that at least one filter is set
     * todo: Reflection can be used to iterate object properies to find at least on e not empty filter para
     */
    public function isFieldFilterSet(): bool
    {
        return !is_null($this->recipient) || !is_null($this->subject) || !is_null($this->sender);
    }

    /** @return callable[] */
    protected function getParamCasters(): array
    {
        $parentCasters = parent::getParamCasters();
        return array_merge($parentCasters, [
            'recipient' => static function ($value) {
                return (string)$value;
            },
            'subject' => static function ($value) {
                return (string)$value;
            },
            'sender' => static function ($value) {
                return (string)$value;
            }
        ]);
    }
}
