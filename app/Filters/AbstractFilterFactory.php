<?php

declare(strict_types=1);

namespace App\Filters;

use App\Filters\BaseFilter;
use App\Filters\Interfaces\FilterFactoryInterface;

abstract class AbstractFilterFactory implements FilterFactoryInterface
{
    abstract protected function createFilter(): BaseFilter;

    public function makeFilterFromRequestData(array $requestData): BaseFilter
    {
        return $this->populateFilter(
            $this->createFilter(),
            $requestData
        );
    }

    protected function populateFilter(BaseFilter $filter, array $filteredRequestData): BaseFilter
    {
        foreach ($filteredRequestData as $paramName => $paramValue) {
            if (property_exists($filter, $paramName) && !empty($paramValue)) {
                $filter->$paramName = $filter->castFilterParam($paramName, $paramValue);
            }
        }
        return $filter;
    }
}
