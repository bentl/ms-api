<?php

declare(strict_types=1);

namespace App\Filters;

class MailFilterFactory extends AbstractFilterFactory
{

    /**
     * @inheritDoc
     */
    protected function createFilter(): MailFilter
    {
        return new MailFilter();
    }
}
