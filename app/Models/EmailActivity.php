<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class EmailActivity extends Model
{
    use HasFactory;

    public const POSTED_STATUS = 'posted';

    public const SENT_STATUS = 'sent';

    public const FAILED_STATUS = 'failed';

    protected $guarded = [];
}
