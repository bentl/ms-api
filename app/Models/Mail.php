<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Support\Arr;

class Mail extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function emailActivity(): HasOne
    {
        return $this->hasOne(EmailActivity::class);
    }

    public function attachments(): HasMany
    {
        return $this->hasMany(Attachment::class);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray()
    {
        $data = parent::toArray();
        $data['htmlContent'] = $data['html_content'];
        $data['textContent'] = $data['text_content'];
        Arr::forget($data, ['text_content', 'html_content']);
        $data['status'] = $this->emailActivity->status ?? EmailActivity::FAILED_STATUS;
        return $data;
    }
}
