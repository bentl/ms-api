<?php

namespace App\Jobs;

use App\Models\EmailActivity;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class EmailSendJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var EmailActivity
     */
    private EmailActivity $emailActivity;

    public function __construct(EmailActivity $emailActivity)
    {
        $this->emailActivity = $emailActivity;
    }

    public function handle()
    {
        try {
            $this->emailActivity->update(
                [
                    'status' => EmailActivity::SENT_STATUS
                ]
            );
        } catch (\Throwable $e) {
            $this->emailActivity->update(
                [
                    'status' => EmailActivity::FAILED_STATUS
                ]
            );
        }
    }
}
