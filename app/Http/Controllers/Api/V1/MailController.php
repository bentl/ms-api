<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Repositories\MailRepository;
use App\Filters\MailFilterFactory;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class MailController extends Controller
{
    private MailRepository $mailRepository;

    private MailFilterFactory $mailFilterFactory;

    public function __construct(
        MailRepository $mailRepository,
        MailFilterFactory $mailFilterFactory
    ) {
        $this->mailRepository = $mailRepository;
        $this->mailFilterFactory = $mailFilterFactory;
    }

    public function index(Request $request)
    {
        return $this->mailRepository->findAllByFilter(
            $this->mailFilterFactory->makeFilterFromRequestData($request->query())
        );
    }

    public function get(string $id)
    {
        $mail = $this->mailRepository->find($id);
        if (is_null($mail)) {
            throw new NotFoundHttpException('Resource is not found.');
        }
        return $mail;
    }
}
