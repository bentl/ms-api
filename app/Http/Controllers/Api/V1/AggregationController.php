<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Services\AggregationService;
use Illuminate\Http\Request;

class AggregationController extends Controller
{
    /**
     * @var AggregationService
     */
    private AggregationService $aggregationService;

    public function __construct(
        AggregationService $aggregationService
    ) {
        $this->aggregationService = $aggregationService;
    }

    public function __invoke(Request $request)
    {
        return $this->aggregationService->getAnalytics();
    }
}
