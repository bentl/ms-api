<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Requests\SendEmailFormRequest;
use App\Services\SendEmailService;
use Illuminate\Http\JsonResponse;

class SendEmailsController extends Controller
{

    private SendEmailService $sendEmailService;

    public function __construct(
        SendEmailService $sendEmailService
    ) {
        $this->sendEmailService = $sendEmailService;
    }

    public function __invoke(SendEmailFormRequest $request): JsonResponse
    {
        $this->sendEmailService->send($request->all());
        return new JsonResponse(null, JsonResponse::HTTP_NO_CONTENT);
    }

}
