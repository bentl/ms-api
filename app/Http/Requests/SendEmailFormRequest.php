<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendEmailFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'sender' => 'required|string|max:255|email',
            'recipient' => 'required|string|max:255|email',
            'subject' => 'required|string|max:78',
            'textContent' => 'string|nullable|max:50000',
            'htmlContent' => 'string|nullable|max:50000',
            'attachments' => 'array',
            'attachments.*' => 'file|max:1024'
        ];
    }
}
