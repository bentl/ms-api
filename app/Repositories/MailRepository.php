<?php

namespace App\Repositories;

use App\Models\Mail;
use App\Filters\BaseFilter;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Arr;

class MailRepository
{
    /**
     * @param BaseFilter $filter
     * @return LengthAwarePaginator
     */
    public function findAllByFilter(BaseFilter $filter): LengthAwarePaginator
    {
        $mails = Mail::query()->orderBy('id', 'desc');
        if ($filter->isFieldFilterSet()) {
            if (isset($filter->recipient)) {
                $mails->where('recipient', 'like', '%' . $filter->recipient . '%');
            }
            if (isset($filter->subject)) {
                $mails->where('subject', 'like', '%' . $filter->subject . '%');
            }
            if (isset($filter->sender)) {
                $mails->where('sender', 'like', '%' . $filter->sender . '%');
            }
        }
        return $mails
            ->paginate($filter->perPage);
    }

    public function find(string $id)
    {
        return Mail::with('Attachments')->find($id);
    }

    public function create(array $data): Mail
    {
        $htmlContent = Arr::get($data, 'htmlContent');
        return Mail::create(
            [
                'sender' => Arr::get($data, 'sender'),
                'recipient' => Arr::get($data, 'recipient'),
                'subject' => Arr::get($data, 'subject'),
                'text_content' => Arr::get(
                    $data,
                    'textContent',
                    fn() => (!is_null($htmlContent) ? strip_tags($htmlContent) : null)
                ),
                'html_content' => $htmlContent,
            ]
        );
    }
}
