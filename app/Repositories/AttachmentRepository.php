<?php

namespace App\Repositories;

use App\Models\Mail;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Str;

class AttachmentRepository
{

    public function create(UploadedFile $attachment, Mail $mail)
    {
        $name = Str::uuid() .$attachment->getClientOriginalName();
        $filePath = $attachment->storeAs('attachments', $name, 'public');
        $mail->attachments()->create([
            'file_path' => $filePath,
            'name' => Str::uuid() . $attachment->getClientOriginalName(),
            'original_name' => $attachment->getClientOriginalName(),
        ]);
    }
}
