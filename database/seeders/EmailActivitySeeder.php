<?php

namespace Database\Seeders;

use App\Models\EmailActivity;
use App\Models\Mail;
use Illuminate\Database\Seeder;

class EmailActivitySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Mail::factory()
            ->count(50)
            ->has(EmailActivity::factory())
            ->create();
    }
}
