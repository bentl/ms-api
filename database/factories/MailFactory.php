<?php

namespace Database\Factories;

use App\Models\Mail;
use Illuminate\Database\Eloquent\Factories\Factory;

class MailFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Mail::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'subject' => $this->faker->sentence('3'),
            'sender' => $this->faker->email,
            'recipient' => $this->faker->email,
            'html_content' => $this->faker->randomHtml(),
            'text_content' => $this->faker->text(),
        ];
    }
}
