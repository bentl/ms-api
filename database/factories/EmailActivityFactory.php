<?php

namespace Database\Factories;

use App\Models\EmailActivity;
use Illuminate\Database\Eloquent\Factories\Factory;

class EmailActivityFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = EmailActivity::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */

    public function definition()
    {
        return [
            'status' => $this->faker->randomElement([
                EmailActivity::FAILED_STATUS,
                EmailActivity::SENT_STATUS
            ]),
        ];
    }
}
